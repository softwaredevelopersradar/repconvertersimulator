﻿using ConverterSimulatorLib;
using ConverterSimulatorLib.Models;
using System;
using System.IO.Ports;
using System.Linq;
using System.Windows;

namespace Test
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ConverterSimulator converterSimulator;

        public MainWindow()
        {
            InitializeComponent();
            Ports.ItemsSource = SerialPort.GetPortNames().ToList();
            Ports.SelectedIndex = 0;

        }

        private void OpenPort_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (converterSimulator != null)
                {
                    converterSimulator.Disconnect();
                    btnPort.Content = "Open port";
                    converterSimulator = null;
                }
                else
                {
                    converterSimulator = new ConverterSimulator();
                    SubscribeToEvents();
                    converterSimulator.Connect(Ports.SelectedItem.ToString(), 9600);
                    btnPort.Content = "Close port";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void SubscribeToEvents()
        {
            converterSimulator.OnConnect += (sender, zero) => {
                DispatchIfNecessary(() =>
                { tbxResult.AppendText("\nOpenPort"); });
            };
            converterSimulator.OnDisconnect += (sender, zero) => {
                DispatchIfNecessary(() =>
                { tbxResult.AppendText("\nClosePort"); });
            };
            converterSimulator.OnConverterRequest += (sender, converter) => {
                DispatchIfNecessary(() =>
                {
                    tbxResult.AppendText($"\nOnConverterRequest: Mode = {converter.Mode.ToString()}  Frequency = {converter.Frequency}");
                });
            };
            converterSimulator.OnCommutatorRequest += (sender, commutator) => {
                DispatchIfNecessary(() =>
                {
                    tbxResult.AppendText($"\nOnConverterRequest: Mode = {commutator.Mode.ToString()}  Antenna = {commutator.Antenna}  Attenuator = {commutator.Attenuator}");
                });
            };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (converterSimulator == null)
                return;
            converterSimulator.SendConverterResponse(ConverterMode.MASTER, Convert.ToInt16(boxFrequency.Text));
        }
    }
}

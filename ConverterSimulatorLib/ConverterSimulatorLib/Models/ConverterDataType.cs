﻿namespace ConverterSimulatorLib.Models
{
    public class ConverterDataType
    {
        public ConverterMode Mode { get; }

        public short Frequency { get; }

        public ConverterDataType(ConverterMode mode, short frequency)
        {
            Mode = mode;
            Frequency = frequency;
        }
    }
}

﻿namespace ConverterSimulatorLib.Models
{
    public enum ConverterMode : byte
    {
        MASTER = 5,
        SLAVE = 6
    }

    public enum CommutatorMode : byte
    {
        MASTER = 3,
        SLAVE = 4
    }
}

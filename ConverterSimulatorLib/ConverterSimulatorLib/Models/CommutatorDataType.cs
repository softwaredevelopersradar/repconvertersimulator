﻿namespace ConverterSimulatorLib.Models
{
    public class CommutatorDataType
    {
        public CommutatorMode Mode { get; }

        public byte Antenna { get; }

        public byte Attenuator { get; }     

        public CommutatorDataType(CommutatorMode mode, byte antenna, byte attenuator)
        {
            Mode = mode;
            Antenna = antenna;
            Attenuator = attenuator;
        }
    }
}

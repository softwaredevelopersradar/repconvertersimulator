﻿using ConverterSimulatorLib.ComPort;
using System;

namespace ConverterSimulatorLib.Models
{
    public class RequestMessage
    {
        #region Const

        public const int LengthHeaderPart = 6;

        public const int LenghtFullMessage = 9;

        public const int LengthDataPart = 3;

        #endregion

        public byte Mode { get; set; }

        public byte ChecksumHeader { get; set; }

        public byte[] DataHeader => new byte[]
        {
            Mode,
            0,
            0x1,
            0,
            0x9
        };

        public byte[] Data { get; set; }

        public byte[] DataBody
        {
            get
            {
                if (Data == null || Data.Length < 3)
                    return Data;

                byte[] data = new byte[Data.Length - 1];
                Array.Copy(Data, data, data.Length);

                return data;
            }
        }

        public byte ChecksumBody { get; set; }

        private bool CheckHeader(byte[] data)
        {
            if (data.Length != DataHeader.Length) return false;

            for (int i = 1; i < data.Length; i++)
            {
                if (data[i] != DataHeader[i])
                    return false;
            }
            return true;
        }

        public static implicit operator RequestMessage(byte[] data)
        {
            if (data == null || data.Length < LengthHeaderPart) return null;

            RequestMessage message = new RequestMessage();
            message.Mode = data[0];
            message.ChecksumHeader = data[5];
            byte[] header = new byte[LengthHeaderPart - 1];
            Array.Copy(data, header, header.Length);

            if (!message.CheckHeader(header))
                return null;

            return message;

        }

        public static explicit operator byte[] (RequestMessage requestMessage)
        {
            byte[] headerData = requestMessage.DataHeader;
            byte checkSumHeader = ChecksumCalculator.CountChecksum(headerData);
            byte checkSumBody = ChecksumCalculator.CountChecksum(requestMessage.Data);

            byte[] responce = new byte[LenghtFullMessage];
            headerData.CopyTo(responce, 0);
            responce[LengthHeaderPart - 1] = checkSumHeader;
            requestMessage.Data.CopyTo(responce, LengthHeaderPart);
            responce[LenghtFullMessage - 1] = checkSumBody;

            return responce;
        }

    }
}

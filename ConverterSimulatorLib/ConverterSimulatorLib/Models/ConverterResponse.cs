﻿using System;

namespace ConverterSimulatorLib.Models
{
    public class ConverterResponse
    {
        public const byte CorrectCheckHeaderResponce = 0xEE;

        private ConverterDataType _convertorData;

        public ConverterResponse(ConverterDataType converterData)
        {
            _convertorData = converterData;
        }

        public byte[] GetBytes()
        {
            byte[] body = BitConverter.GetBytes(_convertorData.Frequency);
            Array.Reverse(body);
            RequestMessage requestMessage = new RequestMessage
            {
                Mode = (byte)_convertorData.Mode,
                Data = body
            };

            return (byte[])requestMessage;
        }

    }
}

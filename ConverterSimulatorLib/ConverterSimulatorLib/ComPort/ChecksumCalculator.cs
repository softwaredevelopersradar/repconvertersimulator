﻿using System;

namespace ConverterSimulatorLib.ComPort
{
    internal static class ChecksumCalculator
    {
        public static byte CountChecksum(byte[] data)
        {
            byte result = 0;
            foreach (byte i in data)
            {
                result += i;
            }
            byte temp = Convert.ToByte(result ^ 0xFF);
            result = Convert.ToByte(temp + 1);

            return result;
        }
    }
}

﻿using System.IO.Ports;

namespace ConverterSimulatorLib.ComPort
{
    public class ComPortModel
    {
        public string ComPortName { get; }

        public Parity Parity { get; } = Parity.None;

        public int DataBits { get; } = 8;

        public StopBits StopBits { get; } = StopBits.One;

        public int BoundRate { get; } = 9600;
        
        private ComPortModel(string comPortName)
        {
            ComPortName = comPortName;
        }
        public ComPortModel(string comPortName, int boundRate)
        {
            ComPortName = comPortName;
            BoundRate = boundRate;
        }
    }
}

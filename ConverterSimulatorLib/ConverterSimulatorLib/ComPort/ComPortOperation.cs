﻿using ConverterSimulatorLib.Models;
using System;
using System.IO.Ports;
using System.Threading;

namespace ConverterSimulatorLib.ComPort
{
    public class ComPortOperation : IComPortOperation
    {
        #region Events
        public event EventHandler<byte[]> OnReadBytes = (obj, args) => { };
        public event EventHandler<byte[]> OnWriteBytes = (obj, args) => { };
        public event EventHandler<RequestMessage> OnGetRequest = (obj, args) => { };
        public event EventHandler OnDisconnect = (obj, args) => { };
        public event EventHandler<string> OnError = (obj, args) => { };
        #endregion

        #region Private

        private SerialPort _serialPort;

        private Thread _thrRead;

        #endregion

        public bool OpenPort(ComPortModel comPortModel)
        {
            if (_serialPort == null)
                _serialPort = new SerialPort();

            if (_serialPort.IsOpen)
                ClosePort();
            try
            {
                // set parameters of port
                _serialPort.PortName = comPortModel.ComPortName;
                _serialPort.BaudRate = comPortModel.BoundRate;

                _serialPort.Parity = comPortModel.Parity;
                _serialPort.DataBits = comPortModel.DataBits;
                _serialPort.StopBits = comPortModel.StopBits;

                _serialPort.RtsEnable = true;
                _serialPort.DtrEnable = true;


                _serialPort.ReceivedBytesThreshold = 1000;

                _serialPort.Open();

                if (_thrRead != null)
                {
                    _thrRead.Abort();
                    _thrRead.Join(500);
                    _thrRead = null;
                }

                try
                {
                    _thrRead = new Thread(new ThreadStart(ReadData));
                    _thrRead.IsBackground = true;
                    _thrRead.Start();
                }
                catch (Exception exp)
                {
                    OnError(this, exp.InnerException?.Message ?? exp.Message);
                }
                return true;
            }
            catch (Exception exp)
            {
                OnError(this, exp.InnerException?.Message ?? exp.Message);
                return false;
            }
        }

        public void ClosePort()
        {
            try
            {
                _serialPort.DiscardInBuffer();
                _serialPort.DiscardOutBuffer();
            }
            catch (Exception exp)
            {
                OnError(this, exp.InnerException?.Message ?? exp.Message);
            }

            try
            {
                _serialPort.Close();

                if (_thrRead != null)
                {
                    _thrRead.Abort();
                    _thrRead.Join(500);
                    _thrRead = null;
                }

                OnDisconnect(this, null);
            }
            catch (Exception exp)
            {
                OnError(this, exp.InnerException?.Message ?? exp.Message);
            }
        }

        public void WriteData(params byte[] data)
        {
            try
            {
                _serialPort.Write(data, 0, data.Length);
                OnWriteBytes(this, data);
            }
            catch (Exception exp)
            {
                OnError(this, exp.InnerException?.Message ?? exp.Message);
            }
        }

        private void ReadData()
        {
            byte[] buffer;
            int iReadByte;

            byte[] bufferSave = null;

            int iBufferSave = 0;

            while (true)
            {
                try
                {
                    buffer = new byte[4000000];

                    iReadByte = _serialPort.Read(buffer, 0, buffer.Length);

                    if (iReadByte > 0)
                    {
                        Array.Resize(ref buffer, iReadByte);
                        OnReadBytes(this, buffer);

                        if (bufferSave == null)
                            bufferSave = new byte[iReadByte];
                        else
                            Array.Resize(ref bufferSave, bufferSave.Length + iReadByte);


                        Array.Copy(buffer, 0, bufferSave, bufferSave.Length - iReadByte, iReadByte);
                        iBufferSave += iReadByte;

                        while (iBufferSave >= RequestMessage.LengthHeaderPart)
                        {
                            RequestMessage message = TrySerializeData(ref bufferSave, ref iBufferSave);
                            if (message != null)
                                OnGetRequest(this, message);
                            else
                                break;
                        }
                    }

                    else
                        OnDisconnect(this, null);
                }

                catch (Exception exp)
                {
                    OnError(this, exp.InnerException?.Message ?? exp.Message);

                    ClosePort();

                }

                Thread.Sleep(5);
            }
        }

        private RequestMessage TrySerializeData(ref byte[] bufferSave, ref int iBufferSave)
        {
            try
            {
                RequestMessage message = bufferSave;
                if (message == null)
                {
                    Array.Resize(ref bufferSave, 0);
                    iBufferSave = 0;
                    return null;
                }

                if (iBufferSave >= RequestMessage.LenghtFullMessage)
                {
                    message.Data = new byte[RequestMessage.LengthDataPart];

                    Array.Copy(bufferSave, RequestMessage.LengthHeaderPart, message.Data, 0, RequestMessage.LengthDataPart);

                    Array.Reverse(bufferSave);
                    Array.Resize(ref bufferSave, bufferSave.Length - RequestMessage.LenghtFullMessage);
                    Array.Reverse(bufferSave);

                    iBufferSave -= RequestMessage.LenghtFullMessage;
                    return message;
                }
                return null;

            }
            catch (Exception exp)
            {
                OnError(this, exp.InnerException?.Message ?? exp.Message);

                Array.Resize(ref bufferSave, 0);
                iBufferSave = 0;
                return null;
            }
        }
    }
}

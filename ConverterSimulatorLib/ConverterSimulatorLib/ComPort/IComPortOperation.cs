﻿using ConverterSimulatorLib.Models;
using System;

namespace ConverterSimulatorLib.ComPort
{
    public interface IComPortOperation
    {
        bool OpenPort(ComPortModel comPortModel);

        void ClosePort();

        void WriteData(params byte[] data);

        event EventHandler<byte[]> OnReadBytes;
        event EventHandler<byte[]> OnWriteBytes;
        event EventHandler OnDisconnect;
        event EventHandler<RequestMessage> OnGetRequest;
    }
}

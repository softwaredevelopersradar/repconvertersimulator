﻿using ConverterSimulatorLib.ComPort;
using ConverterSimulatorLib.Models;
using System;

namespace ConverterSimulatorLib
{
    public class ConverterSimulator
    {
        public ConverterSimulator()
        {
            _comPortOperation = new ComPortOperation();
            _comPortOperation.OnDisconnect += ComPortOperation_OnDisconnect;
            _comPortOperation.OnGetRequest += ComPortOperation_OnGetRequest;
        }

        #region Private vars
        private readonly IComPortOperation _comPortOperation;
        #endregion

        #region Events

        public event EventHandler<ConverterDataType> OnConverterRequest = (obj, args) => { };

        public event EventHandler<CommutatorDataType> OnCommutatorRequest = (obj, args) => { };

        public event EventHandler OnConnect = (obj, args) => { };

        public event EventHandler OnDisconnect = (obj, args) => { };

        #endregion

        #region Event ComPortOperation Handler

        private void ComPortOperation_OnGetRequest(object sender, RequestMessage request)
        {
            Console.WriteLine("OnGetRequest");
            switch (request.Mode)
            {
                case (byte)ConverterMode.MASTER: case (byte)ConverterMode.SLAVE:

                    if (request.ChecksumHeader != ChecksumCalculator.CountChecksum(request.DataHeader))
                        return;
                    _comPortOperation.WriteData(ConverterResponse.CorrectCheckHeaderResponce);

                    var body = request.DataBody;
                    Array.Reverse(body);
                    ConverterDataType convertorData = new ConverterDataType((ConverterMode)request.Mode, BitConverter.ToInt16(body, 0));

                    OnConverterRequest(this, convertorData);

                    break;

                case (byte)CommutatorMode.MASTER: case (byte)CommutatorMode.SLAVE:

                    CommutatorDataType commutatorData = new CommutatorDataType((CommutatorMode)request.Mode, request.Data[0], request.Data[1]);
                    OnCommutatorRequest(this, commutatorData);
                    break;
            }
        }

        private void ComPortOperation_OnDisconnect(object sender, EventArgs e)
        {
            OnDisconnect(sender, e);
        }        
        #endregion

        #region Public Methods

        public void Connect(string comPortName, int boundRate )
        {
            if (_comPortOperation.OpenPort(new ComPortModel(comPortName, boundRate)))
                OnConnect(this, null);
            else
                OnDisconnect(this, null);
        }

        public void Disconnect()
        {
            _comPortOperation.ClosePort();
        }

        public void SendConverterResponse(ConverterMode mode, short frequency)
        {
            ConverterResponse response = new ConverterResponse(new ConverterDataType(mode, frequency));
            _comPortOperation.WriteData(response.GetBytes());
        }

        #endregion
    }
}
